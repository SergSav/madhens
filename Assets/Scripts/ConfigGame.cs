﻿using UnityEngine;

namespace Client
{
    [CreateAssetMenu]
    public class ConfigGame : ScriptableObject
    {
        public int HenStartCount = 5;
        public HenView HenView;
        public FoodView FoodView;
        public Vector2 Offset;
        public int CameraHeight;
    }
}