using Leopotam.Ecs;
using UnityEngine;

namespace Client 
{
    sealed class EcsStartup : MonoBehaviour
    {
        EcsWorld _world;
        EcsSystems _systems;

        public ConfigGame ConfigGame;
        public SceneData SceneData;

        void Start () {
            // void can be switched to IEnumerator for support coroutines.
            
            _world = new EcsWorld ();
            _systems = new EcsSystems (_world);
#if UNITY_EDITOR
            Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create (_world);
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create (_systems);
#endif
            _systems
                .Add(new TimeSystem())
                .Add(new StartSetupSystem())
                .Add(new HenViewCreatorSystem())
                .Add(new FoodViewCreatorSystem())
                .Add(new MovementSystem())
                .Add(new HungerSystem())
                .Add(new CameraControlSystem())
                .Add(new ControlSystem())
                .Add(new GameHUDSystem())
                // register your systems here, for example:
                // .Add (new TestSystem1 ())
                // .Add (new TestSystem2 ())
                
                .OneFrame<UpdateCameraEvent>()
                .OneFrame<TimerSecondEvent>()
                // register one-frame components (order is important), for example:
                // .OneFrame<TestComponent1> ()
                // .OneFrame<TestComponent2> ()
                
                .Inject(ConfigGame)
                .Inject(SceneData)
                .Inject(new ViewPool(ConfigGame.FoodView, ConfigGame.HenView))
                // inject service instances here (order doesn't important), for example:
                // .Inject (new CameraService ())
                // .Inject (new NavMeshSupport ())
                .Init ();
        }

        void Update () {
            _systems?.Run ();
        }

        void OnDestroy () {
            if (_systems != null) {
                _systems.Destroy ();
                _systems = null;
                _world.Destroy ();
                _world = null;
            }
        }
    }
}