﻿using System.Collections.Generic;
using UnityEngine;

namespace Client
{
    public class ViewPool
    {
        private readonly FoodView _foodViewPrefab;
        private List<FoodView> _foodViewPool = new List<FoodView>();
        private List<HenView> _henViewPool = new List<HenView>();
        private HenView _henViewPrefab;
        
        // private List<GameObject> _pool;
        // private GameObject _defaultPrefab;

        public ViewPool(FoodView foodViewPrefab, HenView henViewPrefab)
        {
            _foodViewPrefab = foodViewPrefab;
            _henViewPrefab = henViewPrefab;
            PrepareFoodViewPool();
            PrepareHenViewPool();
        }

        private void PrepareFoodViewPool()
        {
            for (int i = 0; i < 100; i++)
            {
                var view = Object.Instantiate(_foodViewPrefab);
                view.gameObject.SetActive(false);
                _foodViewPool.Add(view);
            }
        }
        
        private void PrepareHenViewPool()
        {
            for (int i = 0; i < 10000; i++)
            {
                var view = Object.Instantiate(_henViewPrefab);
                view.gameObject.SetActive(false);
                _henViewPool.Add(view);
            }
        }

        public FoodView PullFoodView()
        {
            if (_foodViewPool.Count > 0)
            {
                var index = _foodViewPool.Count - 1;
                var view = _foodViewPool[index];
                _foodViewPool.RemoveAt(index);
                view.gameObject.SetActive(true);
                return view;
            }
            return Object.Instantiate(_foodViewPrefab);
        }
        
        public void PushFoodView(FoodView foodView)
        {
            foodView.gameObject.SetActive(false);
            _foodViewPool.Add(foodView);
        }
        
        public HenView PullHenView()
        {
            if (_henViewPool.Count > 0)
            {
                var index = _henViewPool.Count - 1;
                var view = _henViewPool[index];
                _henViewPool.RemoveAt(index);
                view.gameObject.SetActive(true);
                return view;
            }
            return Object.Instantiate(_henViewPrefab);
        }
        
        public void PushHenView(HenView henView)
        {
            henView.gameObject.SetActive(false);
            _henViewPool.Add(henView);
        }

        //
        // public T PullView<T>()
        // {
        //     if (_pool.Count > 0)
        //     {
        //         var index = _pool.Count - 1;
        //         var view = _pool[index];
        //         _pool.RemoveAt(index);
        //         view.gameObject.SetActive(true);
        //         return view;
        //     }
        //     return Object.Instantiate(_defaultPrefab) as T;
        // }
        //
        // public void PushView<T>(T view)
        // {
        //     var go = view as GameObject; 
        //     go.gameObject.SetActive(false);
        //     _pool.Add(go);
        // }
        //
    }
}