﻿using Unity.Mathematics;

namespace Client
{
    public struct Position
    {
        public float3 value;
    }
}