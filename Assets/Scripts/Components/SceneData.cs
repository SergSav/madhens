﻿using UnityEngine;

namespace Client
{
    public class SceneData : MonoBehaviour
    {
        public Transform CameraTransform;
        public Camera Camera;
        public int HenCount;
        public int FoodCount;
        public UI UI;
    }
}