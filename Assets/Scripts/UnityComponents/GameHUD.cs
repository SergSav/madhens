﻿using UnityEngine;
using UnityEngine.UI;

namespace Client
{
    public class GameHUD : MonoBehaviour
    {
        public Text HenCount;
        
        public void SetCurrentHenCount(int count)
        {
            HenCount.text = $"Hens on screen: {count}";
        }
    }
}