﻿using Leopotam.Ecs;
using UnityEngine;

namespace Client
{
    public class FoodViewCreatorSystem : IEcsRunSystem
    {
        private EcsFilter<FoodData, Position>.Exclude<FoodViewRef> _filter;
        private ConfigGame _config;
        private SceneData _sceneData;
        private ViewPool _viewPool;
        
        public void Run()
        {
            foreach (var index in _filter)
            {
                ref var position = ref _filter.Get2(index);
                var foodView = _viewPool.PullFoodView();
                foodView.transform.position = new Vector3(position.value.x, 0, position.value.y);
                _filter.GetEntity(index).Set<FoodViewRef>().view = foodView;

                _sceneData.FoodCount++;
            }
        }
    }
}