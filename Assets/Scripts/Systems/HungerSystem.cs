﻿using Leopotam.Ecs;
using UnityEngine;

namespace Client
{
    public class HungerSystem : IEcsRunSystem
    {
        private EcsWorld _world;
        private SceneData _sceneData;
        private ViewPool _viewPool;
        private int _hungerTimeout = 10;
        private EcsFilter<HenData, HenViewRef> _henDataFilter;
        private EcsFilter<FoodViewRef, Position> _foodFilter;

        public void Run()
        {
            foreach (var index in _henDataFilter)
            {
                ref var henData = ref _henDataFilter.Get1(index);
                ref var henView = ref _henDataFilter.Get2(index);
                // set hunger
                if (Time.time - henData.lastEatTimestamp > _hungerTimeout)
                {
                    henData.isSatisfied = false;
                }

                // eat food
                var eatDistance = 0.2f;
                foreach (var foodIndex in _foodFilter)
                {
                    ref var foodPos = ref _foodFilter.Get2(foodIndex);
                    ref var foodView = ref _foodFilter.Get1(foodIndex);
                    if (Vector3.Distance(henView.view.transform.position,
                        new Vector3(foodPos.value.x, 0, foodPos.value.y)) < eatDistance)
                    {
                        henData.isSatisfied = true;
                        henData.lastEatTimestamp = Time.time;
                        // remove
                        _viewPool.PushFoodView(foodView.view);
                        _foodFilter.GetEntity(foodIndex).Destroy();

                        _sceneData.FoodCount--;
                    }
                }
            }
        }
    }
}