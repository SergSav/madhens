﻿using Leopotam.Ecs;
using UnityEngine;

namespace Client
{
    public class ControlSystem : IEcsRunSystem
    {
        private SceneData _sceneData;
        private EcsWorld _world;
        
        public void Run()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var camera = _sceneData.Camera;
                var targetPos = camera.ScreenToWorldPoint(Input.mousePosition);
                AddFood(new Vector3(targetPos.x, targetPos.z, targetPos.y));
            }
            
            var count = 1;
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.LeftShift))
                count = 1000;
            else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftAlt))
                count = 100;
            else if (Input.GetKey(KeyCode.LeftControl))
                count = 10;

            if (Input.GetKeyDown(KeyCode.A))
            {
                AddHen(count);
            }
        }

        private void AddFood(Vector3 position)
        {
            var foodEntity = _world.NewEntity();
            foodEntity.Set<FoodData>();
            foodEntity.Set<Position>().value = position;
        }

        private void AddHen(int count = 1)
        {
            for (int i = 0; i < count; i++)
            {
                var henEntity = _world.NewEntity();
                henEntity.Set<HenData>();
                henEntity.Set<Position>().value = new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1));
            }
        }
    }
}