﻿using Leopotam.Ecs;
using UnityEngine;

namespace Client
{
    public class CameraControlSystem : IEcsRunSystem
    {
        private EcsFilter<UpdateCameraEvent> _filter;
        private SceneData SceneData;
        private ConfigGame _config;
        
        public void Run()
        {
            if (!_filter.IsEmpty())
            {
                var height = _config.CameraHeight;
                var camera = SceneData.Camera;
                camera.orthographic = true;
                camera.orthographicSize = 10;
                
                SceneData.CameraTransform.position = new Vector3(0, 20,0);
            }
        }
    }
}