﻿using Leopotam.Ecs;

namespace Client
{
    public class GameHUDSystem : IEcsRunSystem
    {
        private SceneData _sceneData;
        
        public void Run()
        {
            _sceneData.UI.GameHUD.SetCurrentHenCount(_sceneData.HenCount);
        }
    }
}