﻿using Leopotam.Ecs;
using UnityEngine;

namespace Client
{
    class StartSetupSystem : IEcsInitSystem
    {
        private ConfigGame _config;
        private EcsWorld _world;
        
        public void Init()
        {
            for (int i = 0; i < _config.HenStartCount; i++)
            {
                var henEntity = _world.NewEntity();
                henEntity.Set<HenData>();
                henEntity.Set<Position>().value = new Vector3(i * Random.Range(-1, 1), 0,i * Random.Range(-1, 1));
            }

            _world.NewEntity().Set<UpdateCameraEvent>();
        }
    }
}