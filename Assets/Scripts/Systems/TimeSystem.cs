﻿using Leopotam.Ecs;

namespace Client
{
    public class TimeSystem : IEcsRunSystem
    {
        private EcsWorld _world;
        private int _tickCount;
        
        public void Run()
        {
            if (_tickCount >= 60)
            {
                _tickCount = 0;
                _world.NewEntity().Set<TimerSecondEvent>();
            }

            _tickCount++;
        }
    }
}