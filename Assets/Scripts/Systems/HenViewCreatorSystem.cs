﻿using Leopotam.Ecs;
using UnityEngine;

namespace Client
{
    public class HenViewCreatorSystem : IEcsRunSystem
    {
        private EcsFilter<HenData, Position>.Exclude<HenViewRef> _filter;
        private ConfigGame _config;
        private SceneData _sceneData;
        private ViewPool _viewPool;
        
        public void Run()
        {
            foreach (var index in _filter)
            {
                ref var position = ref _filter.Get2(index);
                var henView = _viewPool.PullHenView();
                henView.transform.position = new Vector3(position.value.x + _config.Offset.x * position.value.x, 0, position.value.y + _config.Offset.y * position.value.y);
                _filter.GetEntity(index).Set<HenViewRef>().view = henView;

                _sceneData.HenCount++;
            }
        }
    }
}