﻿using Leopotam.Ecs;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Client
{
    public class MovementSystem : IEcsRunSystem
    {
        private EcsWorld _world;
        private EcsFilter<TimerSecondEvent> _timeFilter;
        private EcsFilter<HenViewRef, HenData, Position> _henFilter;
        private EcsFilter<FoodViewRef, Position> _foodFilter;

        public void Run()
        {
            foreach (var index in _henFilter)
            {
                ref var position = ref _henFilter.Get3(index);
                ref var view = ref _henFilter.Get1(index).view;
                view.transform.position = Vector3.Lerp(view.transform.position, position.value, 0.05f);
            }
            
            if (_timeFilter.IsEmpty()) 
                return;
            
            // target decision 
            foreach (var index in _henFilter)
            {
                ref var position = ref _henFilter.Get3(index);
                ref var data = ref _henFilter.Get2(index);
                if (!data.isSatisfied && !_foodFilter.IsEmpty())
                {
                    Vector3 closestFoodPos = Vector3.zero;
                    float minDist = 100000f;
                    foreach (var foodIndex in _foodFilter)
                    {
                        ref var foodPosition = ref _foodFilter.Get2(foodIndex);
                        var foodTransformedPos = new Vector3(foodPosition.value.x, 0, foodPosition.value.y);
                        var dist = Vector3.Distance(position.value, foodTransformedPos);
                        if (dist < minDist)
                        {
                            minDist = dist;
                            closestFoodPos = foodTransformedPos;
                        }
                    } 
                    position.value = closestFoodPos;
                }
                else
                {
                    position.value += new float3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
                }
            }
        }
    }
}